-- Active: 1648455648409@@127.0.0.1@3306@painture
CREATE DATABASE painture;

DROP TABLE if EXISTS type_de_prestation;


CREATE Table type_de_prestation(
id INT PRIMARY KEY AUTO_INCREMENT,
name VARCHAR(64),
image VARCHAR(155),
description VARCHAR(455),
prix INT
);


INSERT INTO type_de_prestation (name,image,description,prix) VALUES (
"image mur",
"image-mur3.jpeg",
"une image ou un dessin peint soit directement sur des murs et des plafonds
 en plâtre, soit sur une toile, du papier ou tout autre matériau fixé à une surface 
 architecturale. Les techniques les plus courantes de peinture murale sont la fresque, 
 la détrempe, la détrempe, l'encaustique et la peinture à l'huile. La peinture murale 
 est un type d'art monumental.",
15),
("image porte",
"image-porte.jpeg",
"une image ou un dessin peint soit directement sur des murs et des plafonds en plâtre, 
soit sur une toile, du papier ou tout autre matériau fixé à une surface architecturale. 
Les techniques les plus courantes de peinture murale sont la fresque, la détrempe, la détrempe, 
l'encaustique et la peinture à l'huile. La peinture murale est un type d'art monumental.",
10),
("image mur et porte",
"image-porte-et-mur.jpeg",
"J’ai tracé des lignes horizontale et verticale suivant le plan de l’exercice
Une fois la teinte repérée j’ai mis les colorants dans la quantité de peinture 
donné J’ai peint les différentes parties du mur avec la teinte la plus claire
Puis j’ai gardé la quantité de peinture pour ma 2eme couche et j’ai reteinté 
le restant de peinture pour trouver la teinte moyenne.",
15);
DROP TABLE if EXISTS image;


CREATE TABLE image(
  id INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(64),
  url VARCHAR(155),
  description TEXT,
  type_de_prestation_id INTEGER,
  FOREIGN KEY (type_de_prestation_id) REFERENCES type_de_prestation(id) 
);

INSERT INTO image (name,url,description,type_de_prestation_id) VALUES (
"image mur",
"image-mur0.jpeg",
"Analyser le chantier
J’ai protégé le chantier avec polyane
Il y avait un trou au plafond
J’ai rebouché le trou avec un petit morceau de placo ensuite j’ai mis 
les bandes puis j’ai enduit la totalité du raccord avec de l’enduit de rebouchage
J’ai ensuite piqué avec un marteau les faïences sur le mur, que j’ai ensuite enduit en 2 passes
J’ai pochoné avec sous-couche les endroits enduit",1),

("image mur",
"image-mur2.jpeg",
"J’ai tracé des lignes horizontale et verticale suivant le plan de l’exercice
Puis j’ai fait une recherche de teinte, suivant la teinte choisie sur le nuancier.
IL faut que je fasse 3 teintes en dégradé et en harmonie
J’ai d’abord recherché les teintes les plus proche de mon modèle.
Puis j’ai pris un peu de peinture satiné acrylique blanche a laquelle j’ai rajouté quelques gouttes de colorants pour trouver exactement la teinte la plus claire",1),

("image mur",
"image-mur5.jpeg",
"Patte de lapin ,Pinceaux Escabeau Polyane
Peinture mat Acrylique
Peinture satiné Acrylique
Papier de verre",1),

("image mur",
"image-mur9.jpeg",
"Pour faire un angle sortant, je prends la mesure de l’emplacement de mon lé jusqu’au retour de l’angle. Je rajoute 1 cm de plus pour faire mon retour d’angle.
Je découpe sur table le lé a la mesure trouvée.
Puis je viens le poser comme vu auparavant.
Avec le restant du même lé, je le pose à 1 mm du bord de mon angle,
SI mon papier dépasse un peu après l’avoir mis à l’aplomb, je l’arase sans coupé le retour de mon 1er lé J’arase en haut et en bas, puis je nettoie",1),

("image porte",
"image-porte3.jpeg",
"J’ai sous-couché le plafond, les murs avec une sous-couche acrylique pour nourrir les fonds (pour qu’ils soient moins absorbant pour la peinture de finition)
Pour la finition A et après séchage, j’ai ratissé l’ensemble de la cabine afin qu’il n’y ai pas de défaut Puis j’ai re sous-couché",2),
("image porte",
"image-porte2.jpeg",
"Puis je viens le poser comme vu auparavant.
Avec le restant du même lé, je le pose à 1 mm du bord de mon angle,
Je prends l’aplomb sur le papier et je maroufle.
SI mon papier dépasse un peu après l’avoir mis à l’aplomb, je l’arase sans coupé le retour de mon 1er lé J’arase en haut et en bas, puis je nettoie",2),

("image mur et porte",
"image-porte-et-mur1.jpeg",
" j’ai pris un peu de peinture satiné acrylique blanche a laquelle j’ai rajouté quelques gouttes de colorants pour trouver exactement la teinte la plus claire
Une fois la teinte repérée j’ai mis les colorants dans la quantité de peinture donné J’ai peint les différentes parties du mur avec la teinte la plus claire
Puis j’ai gardé la quantité de peinture pour ma 2eme couche et j’ai reteinté le restant de peinture pour trouver la teinte moyenne.",3),

("image mur et porte",
"image-porte-et-mur3.jpeg",
"Pour cela je recherche l’endroit le plus creux de mon angle, je prends la cote de mon lé, moins 1cm de retour pour mon angle, puis je prends mon fil à plomb et je fais des petits traits pour marquer mon aplomb, puis je les relis à l’aide de la règle de 2M.
Ce trait d’aplomb me servira de guide pour la pose de mes lés.
Je dépose la colle généreusement sur mon mur, puis je viens afficher mon lé en suivant le trait d’aplomb.",3);



CREATE Table user(
id INT PRIMARY KEY AUTO_INCREMENT,
first_name VARCHAR(64),
last_name VARCHAR(64),
address VARCHAR(125)
);

INSERT INTO user (first_name,last_name,address) VALUES (
  "Abdul",
  "Wadood",
  "4 grande rue part dieu 69003"),
  ("Ali",
  "Khan",
  "7 rue maurice moissonnier 69120"),
   ("jean",
  "cena",
  "7 rue maryse bastie 69008");


CREATE TABLE address(
id INT PRIMARY KEY AUTO_INCREMENT,
address_detail VARCHAR(125),
zipcode INT,
city VARCHAR(55),
country VARCHAR(55)
);

DROP TABLE if EXISTS address;


INSERT INTO address (address_detail,zipcode,city,country) VALUES 
("2, rue de stade","69000","Lyon","France"),
("2 bis, Grande rue","01250","Meximieux","France"),
("2, Gambetta","75000","Paris","France");


DROP TABLE if EXISTS devis;


CREATE TABLE devis(
id INT PRIMARY KEY AUTO_INCREMENT,
name VARCHAR (255),
prix INT,
surface INT,
type_de_prestation_id INTEGER,
FOREIGN KEY (type_de_prestation_id) REFERENCES type_de_prestation(id),
address_id INTEGER, 
FOREIGN KEY (address_id) REFERENCES address(id) 
);


INSERT INTO devis (name,prix,surface,type_de_prestation_id,address_id) VALUES (
  "Mur",15,1,1,1),
  ("Porte",10,1,2,2),
  ("Mur et Porte",15,1,3,3);


DROP TABLE if EXISTS account;

  CREATE Table account(
    id INT PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR (225),
    first_name VARCHAR (225),
    last_name VARCHAR (225),
    password VARCHAR (225),
    role VARCHAR (225)
    

  );

  INSERT INTO account (email, first_name, last_name, role,password) VALUES
("rom@mail.com","Romain","Durant","ROLE_ADMIN","1234"),
("ron@mail.com","Greg","Durant","ROLE_USER","ABCD"),
("ame@mail.com","Amelie","Durant","ROLE_USER","4567"),
("louloute@mail.com","Lou","Durant","ROLE_USER","AERTY");