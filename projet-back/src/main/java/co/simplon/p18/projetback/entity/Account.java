package co.simplon.p18.projetback.entity;

import java.util.Collection;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class Account implements UserDetails  {
  private Integer id;
  @NotBlank
  @Email
  private String email;
  
  private String firstName;
  private String lastName;
  @NotBlank
  private String password;
  @NotNull
  private String role;
  public Account() {
  }
  public Account(@NotBlank @Email String email, String firstName, String lastName, @NotBlank String password,
      @NotNull String role) {
    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
    this.password = password;
    this.role = role;
  }
  public Account(Integer id, @NotBlank @Email String email, String firstName, String lastName,
      @NotBlank String password, @NotNull String role) {
    this.id = id;
    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
    this.password = password;
    this.role = role;
  }
  public Integer getId() {
    return id;
  }
  public void setId(Integer id) {
    this.id = id;
  }
  public String getEmail() {
    return email;
  }
  public void setEmail(String email) {
    this.email = email;
  }
  public String getFirstName() {
    return firstName;
  }
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }
  public String getLastName() {
    return lastName;
  }
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }
  public String getPassword() {
    return password;
  }
  public void setPassword(String password) {
    this.password = password;
  }
  public String getRole() {
    return role;
  }
  public void setRole(String role) {
    this.role = role;
  }
  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return List.of(
      new SimpleGrantedAuthority(role)
  );

  }
  @Override
  public String getUsername() {
    // TODO Auto-generated method stub
    return email;
  }
  @Override
  public boolean isAccountNonExpired() {
    // TODO Auto-generated method stub
    return true;
  }
  @Override
  public boolean isAccountNonLocked() {
    // TODO Auto-generated method stub
    return true;
  }
  @Override
  public boolean isCredentialsNonExpired() {
    // TODO Auto-generated method stub
    return true;
  }
  @Override
  public boolean isEnabled() {
    // TODO Auto-generated method stub
    return true;
  }
 
}
