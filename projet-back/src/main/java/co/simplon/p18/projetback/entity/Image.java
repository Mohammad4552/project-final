package co.simplon.p18.projetback.entity;

public class Image {
    private Integer id;
    private String name;
    private String url;
    private String description;
    public Image() {
    }
    public Image(String name, String url, String description) {
        this.name = name;
        this.url = url;
        this.description = description;
    }
    public Image(Integer id, String name, String url, String description) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.description = description;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    
}
