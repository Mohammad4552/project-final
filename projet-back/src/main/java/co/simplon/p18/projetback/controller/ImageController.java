package co.simplon.p18.projetback.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.p18.projetback.entity.Image;
import co.simplon.p18.projetback.repository.ImageRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/image")
public class ImageController {
    @Autowired
    private ImageRepository imrepo;

    @GetMapping
    public List<Image> getAll() {
        return imrepo.findAll();
    }

    @GetMapping("/{id}")
    public Image getOne(@PathVariable int id) {
        Image image = imrepo.findImageById(id);
        if(image == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return image;
    }

   


    @PutMapping("/{id}")
    public void  updateImage(@PathVariable int id, @Valid @RequestBody Image image) {
         imrepo.updateImage(image);

    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
       
        imrepo.deleteImageById(id);
    }


    @PostMapping
    public void add(@RequestBody @Valid Image image) {
         imrepo.createImage(image);
    }

    @PostMapping("/upload")
    @ResponseStatus(code = HttpStatus.CREATED)
    public Image add(@ModelAttribute Image image, 
                    @ModelAttribute MultipartFile upload) {
        
        image.setId(null);

        
        String filename = UUID.randomUUID() + "."+ FilenameUtils.getExtension(upload.getOriginalFilename());
        
        try {
           
            upload.transferTo(new File(getUploadFolder(), filename));
           
            image.setUrl(filename);
            imrepo.createImage(image);
        } catch (IllegalStateException | IOException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "File upload failed");
        }
        return image;
    }

     
    private File getUploadFolder() {
        File folder = new File(getClass().getClassLoader().getResource(".").getPath().concat("static/uploads"));
        if(!folder.exists()) {
            folder.mkdirs();
        }
        return folder;
            
    }


    @GetMapping("/prest/{id}")
    public  List<Image> getPrestImage(@PathVariable int id) {
        List<Image> image = imrepo.getImageByTypeDePrestationId(id);
        if(image == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return image;

}
}
