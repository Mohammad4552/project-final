package co.simplon.p18.projetback.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.p18.projetback.entity.Address;
import co.simplon.p18.projetback.repository.AddressRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/address")
public class AddressController {

    @Autowired
    private AddressRepository adrepo;

    @GetMapping
    public List<Address> getAll() {
        return adrepo.findAll();
    }

    @GetMapping("/{id}")
    public Address getOne(@PathVariable int id) {
        Address address = adrepo.findAddressById(id);
        if(address == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return address;
    }

   


    @PutMapping("/{id}")
    public void  updateAddress(@PathVariable int id, @Valid @RequestBody Address address) {
         adrepo.updateAddress(address);

    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
       
        adrepo.deleteAddressById(id);
    }


    @PostMapping
    public void add(@RequestBody @Valid Address address) {
         adrepo.createAddress(address);
    }

    
}
