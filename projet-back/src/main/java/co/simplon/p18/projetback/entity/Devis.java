package co.simplon.p18.projetback.entity;

public class Devis {

    private Integer id;
    private String name;
    private Integer prix;
    private Integer surface;
    public Devis() {
    }
    public Devis(String name, Integer prix, Integer surface) {
        this.name = name;
        this.prix = prix;
        this.surface = surface;
    }
    public Devis(Integer id, String name, Integer prix, Integer surface) {
        this.id = id;
        this.name = name;
        this.prix = prix;
        this.surface = surface;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Integer getPrix() {
        return prix;
    }
    public void setPrix(Integer prix) {
        this.prix = prix;
    }
    public Integer getSurface() {
        return surface;
    }
    public void setSurface(Integer surface) {
        this.surface = surface;
    }
   
}
