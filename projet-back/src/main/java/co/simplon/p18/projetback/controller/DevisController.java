package co.simplon.p18.projetback.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.p18.projetback.entity.Devis;
import co.simplon.p18.projetback.repository.DevisRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/devis")
public class DevisController {
    @Autowired
    private DevisRepository derepo;

    @GetMapping
    public List<Devis> getAll() {
        return derepo.findAll();
    }

    @GetMapping("/{id}")
    public Devis getOne(@PathVariable int id) {
        Devis devis = derepo.findDevisById(id);
        if(devis == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return devis;
    }

   


    @PutMapping("/{id}")
    public void  updateDevis(@PathVariable int id, @Valid @RequestBody Devis devis) {
         derepo.updateDevis(devis);

    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
       
        derepo.deleteDevisById(id);
    }


    @PostMapping
    public void add(@RequestBody @Valid Devis devis) {
         derepo.createDevis(devis);
    }

}
