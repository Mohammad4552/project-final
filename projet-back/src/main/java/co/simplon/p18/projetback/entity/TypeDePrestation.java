package co.simplon.p18.projetback.entity;

public class TypeDePrestation {
    private Integer id;
    private String name;
    private String image;
    private String description;
    private Integer prix;
    public TypeDePrestation() {
    }
    public TypeDePrestation(String name, String image, String description, Integer prix) {
        this.name = name;
        this.image = image;
        this.description = description;
        this.prix = prix;
    }
    public TypeDePrestation(Integer id, String name, String image, String description, Integer prix) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.description = description;
        this.prix = prix;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public Integer getPrix() {
        return prix;
    }
    public void setPrix(Integer prix) {
        this.prix = prix;
    }
   
}
