package co.simplon.p18.projetback.entity;

public class Address {
    private Integer id;
    private String address_detail;
    private Integer zipcode;
    private String city;
    private String country;
    public Address() {
    }
    public Address(String address_detail, Integer zipcode, String city, String country) {
        this.address_detail = address_detail;
        this.zipcode = zipcode;
        this.city = city;
        this.country = country;
    }
    public Address(Integer id, String address_detail, Integer zipcode, String city, String country) {
        this.id = id;
        this.address_detail = address_detail;
        this.zipcode = zipcode;
        this.city = city;
        this.country = country;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getAddress_detail() {
        return address_detail;
    }
    public void setAddress_detail(String address_detail) {
        this.address_detail = address_detail;
    }
    public Integer getZipcode() {
        return zipcode;
    }
    public void setZipcode(Integer zipcode) {
        this.zipcode = zipcode;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }
    
}
