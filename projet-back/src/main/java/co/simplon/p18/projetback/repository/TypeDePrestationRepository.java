package co.simplon.p18.projetback.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.management.RuntimeErrorException;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.p18.projetback.entity.TypeDePrestation;

@Repository
public class TypeDePrestationRepository {
  @Autowired
  private DataSource dataSource;

  public List<TypeDePrestation> findAll() {
    List<TypeDePrestation> list = new ArrayList<>();
    try (Connection cnx = dataSource.getConnection()) {
      PreparedStatement stmt = cnx.prepareStatement("SELECT * FROM type_de_prestation");
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        TypeDePrestation typeDePrestation = new TypeDePrestation(
            (rs.getInt("id")),
            (rs.getString("name")),
            (rs.getString("image")),
            (rs.getString("description")),
            (rs.getInt("prix")));
        list.add(typeDePrestation);

      }
    } catch (SQLException e) {
      e.printStackTrace();

    }
    return list;
  }

  public TypeDePrestation findTypeDePrestationById(int id){
    try 
    (Connection cnx = dataSource.getConnection()){      
      PreparedStatement stmt=cnx
      .prepareStatement("SELECT * FROM type_de_prestation where id=?");
      stmt.setInt(1,id);
      ResultSet rs = stmt.executeQuery();
      if(rs.next()){
        TypeDePrestation typeDePrestation=new TypeDePrestation(
          (rs.getInt("id")),
          (rs.getString("name")),
          (rs.getString("image")),
          (rs.getString("description")),
          (rs.getInt("prix")));
          
          return typeDePrestation;
      }

    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return null;
  }


  public void createTypeDePrestation(TypeDePrestation typeDePrestation) {
    try

    (Connection cnx = dataSource.getConnection()) {
      PreparedStatement stmt = cnx.prepareStatement("INSERT INTO type_de_prestation (name, image,description,prix) VALUES (?,?,?,?)",
          Statement.RETURN_GENERATED_KEYS);

      stmt.setString(1, typeDePrestation.getName());
      stmt.setString(2, typeDePrestation.getImage());
      stmt.setString(3, typeDePrestation.getDescription());
      stmt.setInt(4, typeDePrestation.getPrix());
      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        typeDePrestation.setId(rs.getInt(1));
      }

    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      // throw new RuntimeErrorException(e, "repository error");
    }
  }

  public void deleteTypeDePrestationById(int id) {
    try

    (Connection cnx = dataSource.getConnection()) {
      PreparedStatement stmt = cnx.prepareStatement("DELETE FROM type_de_prestation WHERE id= ?");
      stmt.setInt(1, id);
      stmt.executeUpdate();
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

  }

  public void updateTypeDePrestation(TypeDePrestation typeDePrestation){
    try 
    (Connection cnx = dataSource.getConnection()){      
      PreparedStatement stmt=cnx.prepareStatement("UPDATE type_de_prestation SET name= ?, image= ?,description=?, prix=? WHERE id= ?");
      stmt.setString(1, typeDePrestation.getName());
      stmt.setString(2, typeDePrestation.getImage());
      stmt.setString(3, typeDePrestation.getDescription());
      stmt.setInt(4, typeDePrestation.getPrix());
      stmt.setInt(5, typeDePrestation.getId());
      
      stmt.executeUpdate();

    } catch (SQLException e) {
     
      e.printStackTrace();
    }

  }


  


}
