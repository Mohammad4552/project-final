package co.simplon.p18.projetback.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.p18.projetback.entity.Account;
import co.simplon.p18.projetback.repository.AccountRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/account")
public class AccountController {
  
  @Autowired
  private AccountRepository repo;

  @Autowired
    private PasswordEncoder encoder;


  @GetMapping
  public List<Account> getAll() {
      return repo.findAll();
  }

  @GetMapping("/{id}")
  public Account getOne(@PathVariable int id) {
      Account account = repo.findAccountById(id);
      if(account == null) {
          throw new ResponseStatusException(HttpStatus.NOT_FOUND);
      }
      return account;
  }

  @PutMapping("/{id}")
    public void  updateAccount(@PathVariable int id, @Valid @RequestBody Account account) {

         repo.updateAccount(account);

    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
       
        repo.deleteAccountById(id);
    }


    @PostMapping
    public Account add(@RequestBody @Valid Account account) {
        String hash=encoder.encode(account.getPassword());
        account.setPassword(hash);
        account.setRole("ROLE_USER");
         return repo.createAccount(account);
    }

    @GetMapping("/user")
    public Account getAccount(@AuthenticationPrincipal Account account) {
        return account;
    }


  
}
