package co.simplon.p18.projetback.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.p18.projetback.entity.TypeDePrestation;
import co.simplon.p18.projetback.repository.TypeDePrestationRepository;




@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/type_de_prestation")
public class TypeDePrestationController {

    @Autowired
    private TypeDePrestationRepository repo;

    @GetMapping
    public List<TypeDePrestation> getAll() {
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public TypeDePrestation getOne(@PathVariable int id) {
        TypeDePrestation typeDePrestation = repo.findTypeDePrestationById(id);
        if(typeDePrestation == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return typeDePrestation;
    }

   


    @PutMapping("/{id}")
    public void  updateTypeDePrestation(@PathVariable int id, @Valid @RequestBody TypeDePrestation typeDePrestation) {
         repo.updateTypeDePrestation(typeDePrestation);

    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
       
        repo.deleteTypeDePrestationById(id);
    }


    @PostMapping
    public void add(@RequestBody @Valid TypeDePrestation typeDePrestation) {
         repo.createTypeDePrestation(typeDePrestation);
    }


}
