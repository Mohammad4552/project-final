package co.simplon.p18.projetback.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.p18.projetback.entity.Address;

@Repository
public class AddressRepository {
    @Autowired
  private DataSource dataSource;

  public List<Address> findAll() {
    List<Address> list = new ArrayList<>();
    try (Connection cnx = dataSource.getConnection()) {
      PreparedStatement stmt = cnx.prepareStatement("SELECT * FROM address");
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Address address = new Address(
            (rs.getInt("id")),
            (rs.getString("address_detail")),
            (rs.getInt("zipcode")),
            (rs.getString("city")),
            (rs.getString("country")));
        list.add(address);

      }
    } catch (SQLException e) {
      e.printStackTrace();

    }
    return list;
  }

  public Address findAddressById(int id){
    try 
    (Connection cnx = dataSource.getConnection()){      
      PreparedStatement stmt=cnx
      .prepareStatement("SELECT * FROM address where id=?");
      stmt.setInt(1,id);
      ResultSet rs = stmt.executeQuery();
      if(rs.next()){
        Address address=new Address(
            (rs.getInt("id")),
            (rs.getString("address_detail")),
            (rs.getInt("zipcode")),
            (rs.getString("city")),
            (rs.getString("country")));
          
          return address;
      }

    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return null;
  }

  public void createAddress(Address address) {
    try

    (Connection cnx = dataSource.getConnection()) {
      PreparedStatement stmt = cnx.prepareStatement("INSERT INTO address (address_detail, zipcode,city,country) VALUES (?,?,?,?)",
          Statement.RETURN_GENERATED_KEYS);

      stmt.setString(1, address.getAddress_detail());
      stmt.setInt(2, address.getZipcode());
      stmt.setString(3, address.getCity());
      stmt.setString(4, address.getCountry());
      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        address.setId(rs.getInt(1));
      }

    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public void deleteAddressById(int id) {
    try

    (Connection cnx = dataSource.getConnection()) {
      PreparedStatement stmt = cnx.prepareStatement("DELETE FROM address WHERE id= ?");
      stmt.setInt(1, id);
      stmt.executeUpdate();
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

  }

  public void updateAddress(Address address){
    try 
    (Connection cnx = dataSource.getConnection()){      
      PreparedStatement stmt=cnx.prepareStatement("UPDATE address SET address_detail= ?, zipcode= ?, city=?, country=? WHERE id= ?");
      stmt.setString(1, address.getAddress_detail());
      stmt.setInt(2, address.getZipcode());
      stmt.setString(3, address.getCity());
      stmt.setString(4, address.getCountry());
      stmt.setInt(5, address.getId());

      stmt.executeUpdate();

    } catch (SQLException e) {
     
      e.printStackTrace();
    }

  }
}
