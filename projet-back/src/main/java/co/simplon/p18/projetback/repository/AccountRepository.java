package co.simplon.p18.projetback.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.p18.projetback.entity.Account;

@Repository
public class AccountRepository {
  @Autowired
  private DataSource dataSource;

  public List<Account> findAll() {
    List<Account> list = new ArrayList<>();
    try (Connection cnx = dataSource.getConnection()) {
      PreparedStatement stmt = cnx.prepareStatement("SELECT * FROM account");
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Account account = new Account(
            (rs.getInt("id")),
            (rs.getString("email")),
            (rs.getString("first_name")),
            (rs.getString("last_name")),
            (rs.getString("password")),
            (rs.getString("role")));
        list.add(account);

      }
    } catch (SQLException e) {
      e.printStackTrace();

    }
    return list;
  }


  public Account findAccountById(int id){
    try 
    (Connection cnx = dataSource.getConnection()){      
      PreparedStatement stmt=cnx
      .prepareStatement("SELECT * FROM account where id=?");
      stmt.setInt(1,id);
      ResultSet rs = stmt.executeQuery();
      if(rs.next()){
        Account account=new Account(
          (rs.getInt("id")),
            (rs.getString("email")),
            (rs.getString("first_name")),
            (rs.getString("last_name")),
            (rs.getString("password")),
            (rs.getString("role")));
          
          return account;
      }

    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return null;
  }


  public Account createAccount(Account account) {
    try

    (Connection cnx = dataSource.getConnection()) {
      PreparedStatement stmt = cnx.prepareStatement("INSERT account (email, first_name,last_name,password,role) VALUES (?,?,?,?,?)",
          Statement.RETURN_GENERATED_KEYS);

      stmt.setString(1, account.getEmail());
      stmt.setString(2, account.getFirstName());
      stmt.setString(3, account.getLastName());
      stmt.setString(4, account.getPassword());
      stmt.setString(5, account.getRole());
      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        account.setId(rs.getInt(1));
      }
      

    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      // throw new RuntimeErrorException(e, "repository error");
    }
    return account;
  }

  public void deleteAccountById(int id) {
    try

    (Connection cnx = dataSource.getConnection()) {
      PreparedStatement stmt = cnx.prepareStatement("DELETE FROM account WHERE id= ?");
      stmt.setInt(1, id);
      stmt.executeUpdate();
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

  }

  public void updateAccount(Account account){
    try 
    (Connection cnx = dataSource.getConnection()){      
      PreparedStatement stmt=cnx.prepareStatement("UPDATE account SET email= ?, first_name= ?,last_name=?, password=?,role=? WHERE id= ?");
      stmt.setString(1, account.getEmail());
      stmt.setString(2, account.getFirstName());
      stmt.setString(3, account.getLastName());
      stmt.setString(4, account.getPassword());
      stmt.setString(5, account.getRole());
      stmt.setInt(5, account.getId());
      
      stmt.executeUpdate();

    } catch (SQLException e) {
     
      e.printStackTrace();
    }

  }


  public Account findAccountByEmail(String email){
    try 
    (Connection cnx = dataSource.getConnection()){      
      PreparedStatement stmt=cnx
      .prepareStatement("SELECT * FROM account where email=?");
      stmt.setString(1, email);
     
      ResultSet rs = stmt.executeQuery();
      if(rs.next()){
        Account account=new Account(
          (rs.getInt("id")),
            (rs.getString("email")),
            (rs.getString("first_name")),
            (rs.getString("last_name")),
            (rs.getString("password")),
            (rs.getString("role")));
          
          return account;
      }

    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return null;
  }





  
}
  
