package co.simplon.p18.projetback.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import co.simplon.p18.projetback.repository.AccountRepository;

@Service
public class authService implements UserDetailsService {

  @Autowired
  AccountRepository repo;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    
    return repo.findAccountByEmail(username);
  }
  
}
