package co.simplon.p18.projetback.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.p18.projetback.entity.Image;

@Repository
public class ImageRepository {
    @Autowired
    private DataSource dataSource;
  
    public List<Image> findAll() {
      List<Image> list = new ArrayList<>();
      try (Connection cnx = dataSource.getConnection()) {
        PreparedStatement stmt = cnx.prepareStatement("SELECT * FROM image");
        ResultSet rs = stmt.executeQuery();
  
        while (rs.next()) {
           Image image = new Image(
              (rs.getInt("id")),
              (rs.getString("name")),
              (rs.getString("url")),
              (rs.getString("description")));
          list.add(image);
  
        }
      } catch (SQLException e) {
        e.printStackTrace();
  
      }
      return list;
    }
  
    public Image findImageById(int id){
      try 
      (Connection cnx = dataSource.getConnection()){      
        PreparedStatement stmt=cnx
        .prepareStatement("SELECT * FROM image where id=?");
        stmt.setInt(1,id);
        ResultSet rs = stmt.executeQuery();
        if(rs.next()){
          Image image=new Image(
              (rs.getInt("id")),
              (rs.getString("name")),
              (rs.getString("url")),
              (rs.getString("description")));
            
            return image;
        }
  
      } catch (SQLException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
      return null;
    }
  
    public void createImage(Image image) {
      try
  
      (Connection cnx = dataSource.getConnection()) {
        PreparedStatement stmt = cnx.prepareStatement("INSERT INTO image (name, url) VALUES (?,?)",
            Statement.RETURN_GENERATED_KEYS);
  
        stmt.setString(1, image.getName());
        stmt.setString(2, image.getUrl());
        stmt.executeUpdate();
  
        ResultSet rs = stmt.getGeneratedKeys();
        if (rs.next()) {
          image.setId(rs.getInt(1));
        }
  
      } catch (SQLException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
  
    public void deleteImageById(int id) {
      try
  
      (Connection cnx = dataSource.getConnection()) {
        PreparedStatement stmt = cnx.prepareStatement("DELETE FROM image WHERE id= ?");
        stmt.setInt(1, id);
        stmt.executeUpdate();
      } catch (SQLException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
  
    }
  
    public void updateImage(Image image){
      try 
      (Connection cnx = dataSource.getConnection()){      
        PreparedStatement stmt=cnx.prepareStatement("UPDATE image SET name= ?, url= ? WHERE id= ?");
        stmt.setString(1, image.getName());
        stmt.setString(2, image.getUrl());
        stmt.setInt(3, image.getId());
        stmt.executeUpdate();
  
      } catch (SQLException e) {
       
        e.printStackTrace();
      }
  
    }
  

    public List<Image> getImageByTypeDePrestationId(int idPrestation){
      List<Image> list = new ArrayList<>();
      try
      (Connection cnx = dataSource.getConnection()){      
        PreparedStatement stmt=cnx
        .prepareStatement("SELECT * FROM image where type_de_prestation_id=?");
        stmt.setInt(1,idPrestation);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
          Image image = new Image(
             (rs.getInt("id")),
             (rs.getString("name")),
             (rs.getString("url")),
             (rs.getString("description")));
         list.add(image);
 
        }

        return list;
  
      } catch (SQLException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
      return null;
  }
      
}
