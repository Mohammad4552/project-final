package co.simplon.p18.projetback.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.p18.projetback.entity.Devis;




@Repository
public class DevisRepository {
    @Autowired
  private DataSource dataSource;

  public List<Devis> findAll() {
    List<Devis> list = new ArrayList<>();
    try (Connection cnx = dataSource.getConnection()) {
      PreparedStatement stmt = cnx.prepareStatement("SELECT * FROM devis");
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
         Devis dives = new Devis(
            (rs.getInt("id")),
            (rs.getString("name")),
            (rs.getInt("prix")),
            (rs.getInt("surface")));
        list.add(dives);

      }
    } catch (SQLException e) {
      e.printStackTrace();

    }
    return list;
  }

  public Devis findDevisById(int id){
    try 
    (Connection cnx = dataSource.getConnection()){      
      PreparedStatement stmt=cnx
      .prepareStatement("SELECT * FROM devis where id=?");
      stmt.setInt(1,id);
      ResultSet rs = stmt.executeQuery();
      if(rs.next()){
        Devis address=new Devis(
            (rs.getInt("id")),
            (rs.getString("name")),
            (rs.getInt("prix")),
            (rs.getInt("surface")));
          
          return address;
      }

    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return null;
  }

  public void createDevis(Devis devis) {
    try

    (Connection cnx = dataSource.getConnection()) {
      PreparedStatement stmt = cnx.prepareStatement("INSERT INTO devis (name,prix, surface) VALUES (?,?,?)",
          Statement.RETURN_GENERATED_KEYS);

      stmt.setString(1, devis.getName());
      stmt.setInt(2, devis.getPrix());
      stmt.setInt(3, devis.getSurface());
      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        devis.setId(rs.getInt(1));
      }

    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public void deleteDevisById(int id) {
    try

    (Connection cnx = dataSource.getConnection()) {
      PreparedStatement stmt = cnx.prepareStatement("DELETE FROM devis WHERE id= ?");
      stmt.setInt(1, id);
      stmt.executeUpdate();
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

  }

  public void updateDevis(Devis devis){
    try 
    (Connection cnx = dataSource.getConnection()){      
      PreparedStatement stmt=cnx.prepareStatement("UPDATE devis SET name=?, prix= ?, surface= ? WHERE id= ?");
      stmt.setString(1, devis.getName());
      stmt.setInt(2, devis.getPrix());
      stmt.setInt(3, devis.getSurface());
      stmt.setInt(4, devis.getId());

      stmt.executeUpdate();

    } catch (SQLException e) {
     
      e.printStackTrace();
    }

  }
    
}
