import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs';
import { Image } from '../entities';
import { HomeService } from '../home.service';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent implements OnInit {

    image?:Image[];
  
  

  constructor(private hmService:HomeService, private route :ActivatedRoute) { }

  ngOnInit(): void {
    
    this.hmService.getAllImage().subscribe(data=>this.image=data);
    }

  
}
