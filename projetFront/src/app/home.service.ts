import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Image, Prestation } from './entities';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private http: HttpClient) { }

  getAllImage() {
    return this.http.get<Image[]>(environment.apiUrl + 'api/image');
  }

  getImageById(id: number) {
    return this.http.get<Image[]>(environment.apiUrl + 'api/image/' + id);
  }


  getAllPrestations() {
    return this.http.get<Prestation[] >(environment.apiUrl + 'api/type_de_prestation');
  }

  getImageByPrestationId(id: number) {
    return this.http.get<Image[]>(environment.apiUrl + 'api/image/prest/' + id);
  }

  getImage(id:number) {
    return this.http.get<Image[]>(environment.apiUrl+ '/api/type_de_prestation/'+id+'/image');

  }


 

  
}
