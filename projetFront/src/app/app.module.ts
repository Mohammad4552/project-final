import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { AdminPrestationComponent } from './admin-prestation/admin-prestation.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { ImageByPrestationComponent } from './image-by-prestation/image-by-prestation.component';
import { PrestationComponent } from './prestation/prestation.component';
import { ImageComponent } from './image/image.component';
import { LoginComponent } from './login/login.component';
import { DevisListComponent } from './devis-list/devis-list.component';
import { RealLoginComponent } from './real-login/real-login.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FooterComponent } from './footer/footer.component';




@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AdminPrestationComponent,
    HeaderComponent,
    ImageByPrestationComponent,
    PrestationComponent,
    ImageComponent,
    LoginComponent,
    DevisListComponent,
    RealLoginComponent,
    FooterComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    BrowserAnimationsModule
    

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
