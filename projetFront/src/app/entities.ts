export interface Prestation {
    id:number,
    name:string,
    description?:string,
    image?:string,
    prix?:number,
}
export interface Image {
    id?:number,
    name?:string,
    url?:string,
    description?:Text,
  }

  export interface Account{
    id?:number,
    firstName?:string,
    lastName?:string,
    email?:string,
    password?:string,
    role?:string
  }

  export interface Devis{
    id:number,
    name?:string
    prix?:number,
    surface?:number

  }
