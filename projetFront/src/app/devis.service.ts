import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Devis } from './entities';

@Injectable({
  providedIn: 'root'
})
export class DevisService {

  constructor(private http: HttpClient) { }

  getAllDives() {
    return this.http.get<Devis[] >(environment.apiUrl + 'api/devis');
  }
  deleteDevis(id:number)  { 
    return this.http.delete(environment.apiUrl + 'api/devis/'+ id);
  } 

  getDevisById(id:number){     
    return this.http.get<Devis>(environment.apiUrl+'/api/devis/'+id);  
  }


  createDevis(devis:Devis)  { 
    return this.http.post <Devis>(environment.apiUrl + 'api/devis', devis);
  }  

  updateDevisList(id:number, devis:Devis) { 
    return this.http.put(environment.apiUrl + 'api/devis/'+ id,devis);
  }

  updateDevisInList(devis:Devis,id:number){
    return this.http.put(environment.apiUrl+'api/devis/'+id,devis);
  }


}
