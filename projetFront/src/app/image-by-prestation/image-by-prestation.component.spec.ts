import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageByPrestationComponent } from './image-by-prestation.component';

describe('ImageByPrestationComponent', () => {
  let component: ImageByPrestationComponent;
  let fixture: ComponentFixture<ImageByPrestationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImageByPrestationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageByPrestationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
