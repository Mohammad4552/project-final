import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs';
import { Image, Prestation } from '../entities';
import { HomeService } from '../home.service';

@Component({
  selector: 'app-image-by-prestation',
  templateUrl: './image-by-prestation.component.html',
  styleUrls: ['./image-by-prestation.component.css']
})
export class ImageByPrestationComponent implements OnInit {
  prestation?:Prestation[];
  image:Image[] = [];

  constructor(private homeService:HomeService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.pipe(
      switchMap(params => this.homeService.getImageByPrestationId(params['id']))
    ).subscribe(data => this.image = data);
    
    this.route.params.pipe(
      switchMap(params => this.homeService.getImage(params['id']))
    ).subscribe(data => this.image = data);
  }



}
