import { Component, OnInit } from '@angular/core';
import { Prestation } from '../entities';
import { PrestationService } from '../prestation.service';

@Component({
  selector: 'app-prestation',
  templateUrl: './prestation.component.html',
  styleUrls: ['./prestation.component.css']
})
export class PrestationComponent implements OnInit {

  constructor(private service: PrestationService) { }
  prestations?:Prestation[];
  ngOnInit(): void {
    this.service.getAll().subscribe(data=>this.prestations=data);
  }

}
