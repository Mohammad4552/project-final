import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  currentUser = this.auth.currentUser;
  constructor(private auth:AuthService) { }

  ngOnInit(): void {
  }
  logout() {
    this.auth.logout().subscribe();
  }


  
  
}
