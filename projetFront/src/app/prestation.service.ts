import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Prestation } from './entities';

@Injectable({
  providedIn: 'root'
})
export class PrestationService {

  constructor(private http: HttpClient) { }

  getAll() { 
    return this.http.get<Prestation[]>(environment.apiUrl + 'api/type_de_prestation');
  }

  getAllPrestationWithImage() { 
    return this.http.get<Prestation[]>(environment.apiUrl + 'api/type_de_prestation');
  }
  
  getPrestationById(id:number)  { 
    return this.http.get<Prestation>(environment.apiUrl + 'api/type_de_prestation/'+ id);
  }

  deletePrestation(id:number)  { 
    return this.http.delete(environment.apiUrl + 'api/type_de_prestation/'+ id);
  }  

  createPrestation(prestation:Prestation)  { 
    return this.http.post <Prestation>(environment.apiUrl + 'api/type_de_prestation', prestation);
  }  

  updatePrestationList(id:number, prestation:Prestation) { 
    return this.http.put(environment.apiUrl + 'api/type_de_prestation/'+ id,prestation);
  }

  updatePrestationInList(prestation:Prestation,id:number){
    return this.http.put(environment.apiUrl+'api/type_de_prestation/'+id,prestation);
  }

}
