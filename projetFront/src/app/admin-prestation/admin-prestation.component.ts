import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Prestation } from '../entities';
import { PrestationService } from '../prestation.service';

@Component({
  selector: 'app-admin-prestation',
  templateUrl: './admin-prestation.component.html',
  styleUrls: ['./admin-prestation.component.css']
})
export class AdminPrestationComponent implements OnInit {
  currentUser = this.auth.currentUser;
  constructor(private service:PrestationService, private auth:AuthService) { }
  prestations?:Prestation[];
  displayEditForm: boolean = false;
  modifiedprestation: Prestation = { 
    id:0,
    image:'',
    name: '',
    description:'',
    prix:0  

  }

  ngOnInit(): void {
    this.service.getAll().subscribe(data=>this.prestations=data);
  }

  delete(id:number){
    this.service.deletePrestation(id).subscribe(data =>this.delete);
    this.removeFromList(id);
  }  

  removeFromList(id:number) {
    this.prestations?.forEach((prestation, prestations_index)=>{
      if (prestation.id == id) {
        this.prestations?.splice(prestations_index,1)
      }
    })
  }

  prestationId? : number;
  prestation : Prestation= {
    id : 0,
    image:'',
    name: '',
    description:'',
    prix:0  
    
   
  }

  showForm(item:Prestation){
    this.displayEditForm=true;
    this.prestationId=item.id;
    this.modifiedprestation = item;

  }

  createPrestationByName(){
    this.service.createPrestation(this.prestation).subscribe(data=> this.ngOnInit())
  }

  update(prestation:Prestation) {
    this.service.updatePrestationInList(prestation,this.prestationId!).subscribe();
    this.updatePrestationInList(this.prestations!);
    this.displayEditForm = false;

  }

  updatePrestationInList(prestation:Prestation[]) {
    prestation.forEach((ct_list) => {
      if (ct_list.id == this.prestation.id) {
        ct_list.name = this.prestation.name;
        

      }
    });

  }
}
