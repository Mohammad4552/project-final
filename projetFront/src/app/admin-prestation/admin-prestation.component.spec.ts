import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPrestationComponent } from './admin-prestation.component';

describe('AdminPrestationComponent', () => {
  let component: AdminPrestationComponent;
  let fixture: ComponentFixture<AdminPrestationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminPrestationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPrestationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
