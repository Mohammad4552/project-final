import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs';
import { AuthService } from '../auth.service';
import { DevisService } from '../devis.service';
import { Devis } from '../entities';

@Component({
  selector: 'app-devis-list',
  templateUrl: './devis-list.component.html',
  styleUrls: ['./devis-list.component.css']
})
export class DevisListComponent implements OnInit {
  currentUser = this.auth.currentUser;
  constructor(private dvService:DevisService, private route:ActivatedRoute, private auth:AuthService) { }


  devis?:Devis[];
  
  devisToCreate:Devis={
    id:0,
    name:'',
    prix: 0,
    surface:0    
  }


  ngOnInit(): void {
    
     this.dvService.getAllDives().subscribe(data=>this.devis=data);
    }
       
    fetchPrestations(){ 
      this.dvService.getAllDives().subscribe(data => this.devis = data);
    }


    getDevisById(id:number){
      this.dvService.getDevisById(id).subscribe((data: any)=>this.devis=data)
    }
    createDevis(){
      this.dvService.createDevis(this.devisToCreate).subscribe(data=> this.fetchPrestations());
    }
    devisId? : number;
    displayEditForm: boolean = false;
    modifieddevis: Devis = { 
      id:0,
      name:'',
      prix: 0,
      surface:0
  
    }

    showForm(item:Devis){
      this.displayEditForm=true;
      this.devisId=item.id;
      this.modifieddevis = item;
  
    }
    delete(id:number){
      this.dvService.deleteDevis(id).subscribe(data =>this.delete);
      this.removeFromList(id);
    }  

    removeFromList(id:number) {
      this.devis?.forEach((devis, devis_index)=>{
        if (devis.id == id) {
          this.devis?.splice(devis_index,1)
        }
      })
    }

    update(devis:Devis) {
      this.dvService.updateDevisInList(devis,this.devisId!).subscribe();
      this.updateDevisInList(this.devis!);
      this.displayEditForm = false;
  
    }
  
    updateDevisInList(devis:Devis[]) {
      devis.forEach((ct_list) => {
        if (ct_list.id == this.devisToCreate.id) {
          ct_list.name = this.devisToCreate.name;
          ct_list.prix = this.devisToCreate.prix;
          ct_list.surface = this.devisToCreate.surface;
          
  
        }
      });
  
    }

}
