import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';
import {  Prestation } from '../entities';
import { HomeService } from '../home.service';
import { Image } from '../entities';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  prestations?:Prestation[];

  image?:Image[];
 

  constructor(private hmServvice:HomeService, private route :ActivatedRoute) { }

  ngOnInit(): void {
    this.route.queryParamMap.pipe(
      switchMap(parms =>{ 
       
        return this.hmServvice.getAllPrestations() 
      })
     ).subscribe(data =>{ 
       this.prestations = data
     });
     this.route.queryParamMap.pipe(
      switchMap(parms =>{ 
       
        return this.hmServvice.getAllImage() 
      })
     ).subscribe(data =>{ 
       this.image = data
     });
    }


    changeUrlIfUploadFile(value:string):string{
      if(value.startsWith('http')) {
        return value;
      }
      return environment.apiUrl + '/upload' + value;
    }

  fetchPrestations(){ 
    this.hmServvice.getAllPrestations().subscribe(data => this.prestations = data);
  }

}
