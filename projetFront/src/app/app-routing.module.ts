import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminPrestationComponent } from './admin-prestation/admin-prestation.component';
import { DevisListComponent } from './devis-list/devis-list.component';
import { HomeComponent } from './home/home.component';
import { ImageByPrestationComponent } from './image-by-prestation/image-by-prestation.component';
import { ImageComponent } from './image/image.component';
import { LoginComponent } from './login/login.component';

import { PrestationComponent } from './prestation/prestation.component';
import { RealLoginComponent } from './real-login/real-login.component';

const routes: Routes = [
  {path:'',component:HomeComponent },
  {path:'home',component:HomeComponent},
  {path:'admin-prestation',component:AdminPrestationComponent },
  {path:'image/:id',component:ImageByPrestationComponent },
  {path:'prestation',component:PrestationComponent },
  {path:'image',component:ImageComponent },
  {path:'login',component:LoginComponent},
  {path:'devis',component:DevisListComponent},
  {path:'connect',component:RealLoginComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
